package dto;

public class Password {
	
	final int longitudD=8;
	
	//atributs
	private int longitud;
	private String contrase�a;
	
	//constructors
	public Password() {
		this.longitud=longitudD;
		this.contrase�a=generarPassword(this.longitud);
	}

	public Password(int longitud) {
		this.longitud = longitud;
		this.contrase�a = generarPassword(this.longitud);
	}
	
	//metodes
	public String generarPassword(int longitud) {
		String abecedari="QWERTYUIOPASDFGHJKL�ZXCVBNMqwertyuiopasdfghjkll�zxcvbnm";
		String contra="";
		for(int i=0;i<longitud;i++) {
			int elegir=(int) (Math.random()*(10-0+1)+0);
			if(elegir>=5) {
				int num=(int) (Math.random()*(9-0+1)+0);
				contra=contra+num;
			} else {
				elegir=(int) (Math.random()*(54-0+1)+0);
				char lletra=abecedari.charAt(elegir);
				contra=contra+lletra;
			}
		}
		return contra;
	}
	
	public boolean esFuerte() {
		String contra=this.contrase�a;
		int contadorM=0;
		int contadorm=0;
		int contadorN=0;
		boolean segur=false;
		String mayus="QWERTYUIOPASDFGHJKL�ZXCVBNM";
		String minus="qwertyuiopasdfghjkl�zxcvbnm";
		for(int i=0; i<contra.length();i++) {
			boolean isMayus=false;
			boolean isMinus=false;
			for(int o=0; o<mayus.length();o++) {
				if(contra.charAt(i) == mayus.charAt(o)) {
					isMayus=true;
				} else if(contra.charAt(i) == minus.charAt(o)) {
					isMinus=true;
				}
			}
			boolean isNum=false;
			for(int o=0;o<10;o++) {
				int num = contra.charAt(i) - '0';
				if(num==o) {
					isNum=true;
				}
			}
			if(isNum) {
				contadorN++;
			} else if(isMayus) {
				contadorM++;
			} else if(isMinus) {
				contadorm++;
			}
		}
		//System.out.println("mayus:"+contadorM+" minus:"+contadorm+" num:"+contadorN);
		if(contadorm>=1 && contadorM>=2 && contadorN>=5) {
			segur=true;
		}
		return segur;
	}

	
	//geters i seters
	public int getLongitud() {
		return longitud;
	}

	public String getContrase�a() {
		return contrase�a;
	}

	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	
	
	
	

}
