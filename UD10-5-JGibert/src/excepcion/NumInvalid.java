package excepcion;

public class NumInvalid extends Exception {
	private int codigoError;
	
    
    public NumInvalid (int codigoError) {
		super();
		this.codigoError = codigoError;
	}


	@Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(codigoError){
            case 0:
                mensaje="El numero minimo es 2";
                break;
            case 1:
                mensaje="El numero minimo es 1";
                break;
        }
         
        return mensaje;
         
    }
    
}
