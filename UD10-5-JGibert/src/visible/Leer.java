package visible;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Leer {

	Scanner scan = new Scanner(System.in);
	
	public int scanInt() {
		int num=0;
		boolean sortir=false;
		while(sortir==false)
		try {
			System.out.print("Introduco un numero entero: ");
			num=scan.nextInt();
			sortir=true;
		} catch(InputMismatchException e) {
			System.out.println("No has introducido un numero entero.");
			scan = new Scanner(System.in);
			sortir=false;
		}
		return num;
	}
}
