package excepcion;

public class EsPar extends Exception {
	private int codigoError;
	
    
    public EsPar(int codigoError) {
		super();
		this.codigoError = codigoError;
	}


	@Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(codigoError){
            case 1:
                mensaje="Es impar";
                break;
            case 0:
                mensaje="Es par";
                break;
        }
         
        return mensaje;
         
    }
    
}
