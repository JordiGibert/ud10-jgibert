import dto.Random;
import excepcion.EsPar;

public class MainApp {

	
	static Random numAleatori= new Random();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			int i= numAleatori.randomInt(1,999);
			System.out.println("El numero aleatorio generado es: "+i);
			if((i%2)==0) {
				throw new EsPar(0);
			} else {
				throw new EsPar(1);
			}
			} catch (EsPar ex) {
				System.out.println(ex.getMessage());
		}
	}
}
