package dto;
import visible.*;
import java.util.InputMismatchException;

public class Partida {

	//atributs
	private int numA;
	private int intents;
	private boolean victoria;
	
	//Constructor
	public Partida() {
		this.numA=(int) (Math.random()*500+1);
		this.intents=0;
		this.victoria=false;
	}
	
	//Getters i setters
	public int getIntents() {
		return intents;
	}
	public boolean isVictoria() {
		return victoria;
	}
	public void setIntents(int intents) {
		this.intents = intents;
	}
	
	//metodes propis
	//
	
	public void ferIntent() {
		try {
			ScanInt a =new ScanInt();
			int intent = a.provarSort();
			if(intent==this.numA) {
				this.victoria=true;
			} else if(intent>this.numA) {
				System.out.println(intent+" es un nombre m�s elevat que el que busques. Sort a la proxima!");
			} else {
				System.out.println(intent+" es un nombre m�s petit que el que busques, sort a la proxima!");
			}
		} catch(InputMismatchException e) {
			System.out.println("No has introduit un nombre");
		} finally {
			this.intents++;
		}
		if(this.victoria) {
			System.out.println("Has tobat el nombre! Has fet "+this.intents+" intents");
		}
	}
	
}
