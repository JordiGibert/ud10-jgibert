package visible;

import java.util.Scanner;

import exception.EsPatata;

public class Scan {


	public void llegirPatata() {
		System.out.print("Provem si saps escriure correctament, escriu 'Patata': ");
		Scanner scan = new Scanner(System.in);
		try {
			String intent=scan.next();
			if(intent.equals("Patata")) {
				throw new EsPatata(0);
			} else {
				throw new EsPatata(1);
			}
		} catch(EsPatata a) {
			System.out.println(a.getMessage());
		}
	}
	

}
