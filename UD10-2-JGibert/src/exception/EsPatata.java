package exception;

public class EsPatata extends Exception{
	private int codigoError;
	
    
    public EsPatata(int codigoError) {
		super();
		this.codigoError = codigoError;
	}


	@Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(codigoError){
            case 1:
                mensaje="Aixo no es Patata, no has passat la prova";
                break;
            case 0:
            	mensaje="Has escrit Patata, ets un geni!";
        }
         
        return mensaje;
         
    }
}
