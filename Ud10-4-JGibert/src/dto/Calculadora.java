package dto;

import excepcion.DividirZero;
import visible.Menu;

public class Calculadora {
	Menu menu=new Menu();
	public void ejecutarCalculadora() {
		int numMenu=0;
		while(numMenu!=8) {
			numMenu =menu.mostrarMenu();
			switch(numMenu) {
				case 1:
					System.out.println("--------------------------------------\nEl resultado de la suma es: "+suma(menu.pedirDigitos())+"\n--------------------------------------");
					break;
				case 2:
					System.out.println("--------------------------------------\nEl resultado de la resta es: "+resta(menu.pedirDigitos())+"\n--------------------------------------");
					break;
				case 3:
					System.out.println("--------------------------------------\nEl resultado de la multiplicacion es: "+multiplicacion(menu.pedirDigitos())+"\n--------------------------------------");
					break;
				case 4:
					System.out.println("--------------------------------------\nEl resultado de la potencia es: "+potencia(menu.pedirDigitos())+"\n--------------------------------------");
					break;
				case 5:
					System.out.println("--------------------------------------\nEl resultado de la raiz cuadrada es: "+raizQuadrada(menu.pedirDigito())+"\n--------------------------------------");
					break;
				case 6:
					System.out.println("--------------------------------------\nEl resultado de la raiz cubica es: "+raizCubica(menu.pedirDigito())+"\n--------------------------------------");
					break;
				case 7:
					System.out.println("--------------------------------------\nEl resultado de la division es: "+division(menu.pedirDigitos())+"\n--------------------------------------");
					break;
				case 8:
					System.out.println("\n\tAPAGANDO CALCULADORA");
					
			}
		}
	}
	
	private double suma(double[] nums) {
		return nums[0]+nums[1];
		
	}
	
	private double resta(double[] nums) {
		return nums[0]-nums[1];
		
	}
	
	private double multiplicacion(double[] nums) {
		return nums[0]*nums[1];
		
	}
	
	private double division(double[] nums) {
		double division= 0.0;
		try {
			double divisor=nums[1];
			if(divisor==0) {
				throw new DividirZero(0);
			} else {
				division=nums[0]/nums[1];
			}
		} catch(DividirZero a) {
			System.out.println(a.getMessage());
		}
		return division;
	}
	
	private double potencia(double[] nums) {
		double potencia=nums[0];
		for (int i=1;i<nums[1];i++) {
			potencia=potencia*nums[0];
		}
		return potencia;
		
	}
	
	private double raizQuadrada(double num) {
		try {
			if(num<0) {
				throw new DividirZero(1);
			}
		}catch(DividirZero ex) {
			System.out.println(ex.getMessage());
			num=num*(-1);
		}
		return Math.sqrt(num);
	}
	
	private double raizCubica(double num) {
		return Math.cbrt(num);
	}
	
	
}
