package visible;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
	Scanner scan = new Scanner(System.in);
	public int mostrarMenu() {
		int menu=0;
		do {
			try {
				System.out.println("�Que operacion quieres hacer?"
						+ "\n\t 1- Suma"
						+ "\n\t 2- Resta"
						+ "\n\t 3- Multiplicacio"
						+ "\n\t 4- Potencia"
						+ "\n\t 5- Raiz cuadrada"
						+ "\n\t 6- Raiz cubica"
						+ "\n\t 7- Division"
						+ "\n\t 8- Salir");
				menu=scan.nextInt();
				if(menu>8 || menu<1) {
					System.out.println("El numero introducido no esta en el menu");
				}
			} catch(InputMismatchException e) {
				System.out.println("No has introdocido un digito");
				menu=0;
				scan = new Scanner(System.in);
			}
		} while(menu>8 || menu<1);
		
		return menu;
	}
	
	
	public double[] pedirDigitos() {
		double[] digitos = new double[2];
		boolean error=false;
		do {
			try {
				System.out.print("Introduce el primer digito: ");
				digitos[0]=scan.nextDouble();
				System.out.print("Introduce el segundo digito: ");
				digitos[1]=scan.nextDouble();
				error=false;
			} catch(InputMismatchException e) {
				System.out.println("No has introdocido un digito");
				scan = new Scanner(System.in);
				error=true;
			}
		} while(error==true);
		return digitos;
	}
	
	public double pedirDigito() {
		double digito=0;
		boolean error=false;
		do {
			try {
				System.out.print("Introduce el digito: ");
				digito=scan.nextDouble();
				error=false;
			} catch(InputMismatchException e) {
				System.out.println("No has introdocido un digito");
				scan = new Scanner(System.in);
				error=true;
			}
		} while(error==true);
		return digito;
	}
	
}
