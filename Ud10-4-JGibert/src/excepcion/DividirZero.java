package excepcion;

public class DividirZero extends Exception {
	private int codigoError;
	
    
    public DividirZero(int codigoError) {
		super();
		this.codigoError = codigoError;
	}


	@Override
    public String getMessage(){
         
        String mensaje="";
         
        switch(codigoError){
            case 0:
                mensaje="No se puede dividir entre 0";
                break;
            case 1:
            	mensaje="No puedes hacer raizes cuadradas de un numero negativo";
            	break;
        }
         
        return mensaje;
         
    }
    
}